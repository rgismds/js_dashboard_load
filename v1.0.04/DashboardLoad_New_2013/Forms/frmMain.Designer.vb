﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatabaseConfigToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvalidSubCatCheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UtilitiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatchRebuildDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunWeeklyStoredProcedureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunClothingStoredProcedureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.gbDates = New System.Windows.Forms.GroupBox()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.txtEndDate = New System.Windows.Forms.TextBox()
        Me.txtStartDate = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gbData = New System.Windows.Forms.GroupBox()
        Me.lvData = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuExceptions = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearExceptionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.EmergencyCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.C1FullCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.C2CountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TACountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.RemoveDupeDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.labelStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tspb = New System.Windows.Forms.ToolStripProgressBar()
        Me.bgwRun = New System.ComponentModel.BackgroundWorker()
        Me.bgwLoadFiles = New System.ComponentModel.BackgroundWorker()
        Me.btnDeDupe = New System.Windows.Forms.Button()
        Me.AdditionalCountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.gbDates.SuspendLayout()
        Me.gbData.SuspendLayout()
        Me.mnuExceptions.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.UtilitiesToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(848, 28)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatabaseConfigToolStripMenuItem, Me.InvalidSubCatCheckToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(44, 24)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'DatabaseConfigToolStripMenuItem
        '
        Me.DatabaseConfigToolStripMenuItem.Name = "DatabaseConfigToolStripMenuItem"
        Me.DatabaseConfigToolStripMenuItem.Size = New System.Drawing.Size(222, 26)
        Me.DatabaseConfigToolStripMenuItem.Text = "&Database Config"
        '
        'InvalidSubCatCheckToolStripMenuItem
        '
        Me.InvalidSubCatCheckToolStripMenuItem.Name = "InvalidSubCatCheckToolStripMenuItem"
        Me.InvalidSubCatCheckToolStripMenuItem.Size = New System.Drawing.Size(222, 26)
        Me.InvalidSubCatCheckToolStripMenuItem.Text = "Invalid SubCat Check"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(222, 26)
        Me.OptionsToolStripMenuItem.Text = "&Options"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(222, 26)
        Me.ExitToolStripMenuItem.Text = "&Exit"
        '
        'UtilitiesToolStripMenuItem
        '
        Me.UtilitiesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BatchRebuildDataToolStripMenuItem, Me.RunWeeklyStoredProcedureToolStripMenuItem, Me.RunClothingStoredProcedureToolStripMenuItem})
        Me.UtilitiesToolStripMenuItem.Name = "UtilitiesToolStripMenuItem"
        Me.UtilitiesToolStripMenuItem.Size = New System.Drawing.Size(71, 24)
        Me.UtilitiesToolStripMenuItem.Text = "Utilities"
        '
        'BatchRebuildDataToolStripMenuItem
        '
        Me.BatchRebuildDataToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.DataCollection
        Me.BatchRebuildDataToolStripMenuItem.Name = "BatchRebuildDataToolStripMenuItem"
        Me.BatchRebuildDataToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.BatchRebuildDataToolStripMenuItem.Text = "Batch Rebuild Data"
        '
        'RunWeeklyStoredProcedureToolStripMenuItem
        '
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Calendar
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Name = "RunWeeklyStoredProcedureToolStripMenuItem"
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.RunWeeklyStoredProcedureToolStripMenuItem.Text = "Run Weekly Stored Procedure"
        '
        'RunClothingStoredProcedureToolStripMenuItem
        '
        Me.RunClothingStoredProcedureToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.LayerUnfreeze
        Me.RunClothingStoredProcedureToolStripMenuItem.Name = "RunClothingStoredProcedureToolStripMenuItem"
        Me.RunClothingStoredProcedureToolStripMenuItem.Size = New System.Drawing.Size(288, 26)
        Me.RunClothingStoredProcedureToolStripMenuItem.Text = "Run Clothing Stored Procedure"
        '
        'gbDates
        '
        Me.gbDates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbDates.Controls.Add(Me.btnLoad)
        Me.gbDates.Controls.Add(Me.dtpEnd)
        Me.gbDates.Controls.Add(Me.dtpStart)
        Me.gbDates.Controls.Add(Me.txtEndDate)
        Me.gbDates.Controls.Add(Me.txtStartDate)
        Me.gbDates.Controls.Add(Me.Label2)
        Me.gbDates.Controls.Add(Me.Label1)
        Me.gbDates.Location = New System.Drawing.Point(16, 33)
        Me.gbDates.Margin = New System.Windows.Forms.Padding(4)
        Me.gbDates.Name = "gbDates"
        Me.gbDates.Padding = New System.Windows.Forms.Padding(4)
        Me.gbDates.Size = New System.Drawing.Size(816, 143)
        Me.gbDates.TabIndex = 1
        Me.gbDates.TabStop = False
        Me.gbDates.Text = "Load Dates"
        '
        'btnLoad
        '
        Me.btnLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLoad.Location = New System.Drawing.Point(711, 100)
        Me.btnLoad.Margin = New System.Windows.Forms.Padding(4)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(75, 28)
        Me.btnLoad.TabIndex = 6
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'dtpEnd
        '
        Me.dtpEnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpEnd.Location = New System.Drawing.Point(760, 68)
        Me.dtpEnd.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(24, 22)
        Me.dtpEnd.TabIndex = 5
        '
        'dtpStart
        '
        Me.dtpStart.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpStart.Location = New System.Drawing.Point(760, 36)
        Me.dtpStart.Margin = New System.Windows.Forms.Padding(4)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(24, 22)
        Me.dtpStart.TabIndex = 4
        '
        'txtEndDate
        '
        Me.txtEndDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEndDate.Location = New System.Drawing.Point(93, 68)
        Me.txtEndDate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtEndDate.Name = "txtEndDate"
        Me.txtEndDate.ReadOnly = True
        Me.txtEndDate.Size = New System.Drawing.Size(657, 22)
        Me.txtEndDate.TabIndex = 3
        '
        'txtStartDate
        '
        Me.txtStartDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStartDate.Location = New System.Drawing.Point(93, 36)
        Me.txtStartDate.Margin = New System.Windows.Forms.Padding(4)
        Me.txtStartDate.Name = "txtStartDate"
        Me.txtStartDate.ReadOnly = True
        Me.txtStartDate.Size = New System.Drawing.Size(657, 22)
        Me.txtStartDate.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 71)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "End Date:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 39)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Start Date:"
        '
        'gbData
        '
        Me.gbData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbData.Controls.Add(Me.lvData)
        Me.gbData.Location = New System.Drawing.Point(16, 183)
        Me.gbData.Margin = New System.Windows.Forms.Padding(4)
        Me.gbData.Name = "gbData"
        Me.gbData.Padding = New System.Windows.Forms.Padding(4)
        Me.gbData.Size = New System.Drawing.Size(816, 410)
        Me.gbData.TabIndex = 2
        Me.gbData.TabStop = False
        Me.gbData.Text = "Data"
        '
        'lvData
        '
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader6, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader7})
        Me.lvData.ContextMenuStrip = Me.mnuExceptions
        Me.lvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvData.FullRowSelect = True
        Me.lvData.Location = New System.Drawing.Point(4, 19)
        Me.lvData.Margin = New System.Windows.Forms.Padding(4)
        Me.lvData.Name = "lvData"
        Me.lvData.Size = New System.Drawing.Size(808, 387)
        Me.lvData.TabIndex = 0
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Tag = "2"
        Me.ColumnHeader1.Text = "Inv Date"
        Me.ColumnHeader1.Width = 71
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Tag = "1"
        Me.ColumnHeader2.Text = "Store No"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Tag = "0"
        Me.ColumnHeader6.Text = "Store Type"
        Me.ColumnHeader6.Width = 69
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Tag = "0"
        Me.ColumnHeader3.Text = "Exception"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Tag = "0"
        Me.ColumnHeader4.Text = "Table"
        Me.ColumnHeader4.Width = 101
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Tag = "0"
        Me.ColumnHeader5.Text = "Duplicate"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Dupe Date"
        Me.ColumnHeader7.Width = 109
        '
        'mnuExceptions
        '
        Me.mnuExceptions.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.mnuExceptions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearExceptionToolStripMenuItem, Me.ToolStripSeparator1, Me.EmergencyCountToolStripMenuItem, Me.C1FullCountToolStripMenuItem, Me.C2CountToolStripMenuItem, Me.TACountToolStripMenuItem, Me.TrialToolStripMenuItem, Me.AdditionalCountToolStripMenuItem, Me.ToolStripSeparator2, Me.RemoveDupeDataToolStripMenuItem})
        Me.mnuExceptions.Name = "mnuExceptions"
        Me.mnuExceptions.Size = New System.Drawing.Size(283, 272)
        '
        'ClearExceptionToolStripMenuItem
        '
        Me.ClearExceptionToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Clear
        Me.ClearExceptionToolStripMenuItem.Name = "ClearExceptionToolStripMenuItem"
        Me.ClearExceptionToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.ClearExceptionToolStripMenuItem.Text = "Clear Exception"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(279, 6)
        '
        'EmergencyCountToolStripMenuItem
        '
        Me.EmergencyCountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Decline_v2
        Me.EmergencyCountToolStripMenuItem.Name = "EmergencyCountToolStripMenuItem"
        Me.EmergencyCountToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.EmergencyCountToolStripMenuItem.Text = "Emergency Count"
        '
        'C1FullCountToolStripMenuItem
        '
        Me.C1FullCountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File3tick
        Me.C1FullCountToolStripMenuItem.Name = "C1FullCountToolStripMenuItem"
        Me.C1FullCountToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.C1FullCountToolStripMenuItem.Text = "C1 / Full Count"
        '
        'C2CountToolStripMenuItem
        '
        Me.C2CountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File2tickfade
        Me.C2CountToolStripMenuItem.Name = "C2CountToolStripMenuItem"
        Me.C2CountToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.C2CountToolStripMenuItem.Text = "C2 Count"
        '
        'TACountToolStripMenuItem
        '
        Me.TACountToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.File5tick
        Me.TACountToolStripMenuItem.Name = "TACountToolStripMenuItem"
        Me.TACountToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.TACountToolStripMenuItem.Text = "TA Count"
        '
        'TrialToolStripMenuItem
        '
        Me.TrialToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.Question1
        Me.TrialToolStripMenuItem.Name = "TrialToolStripMenuItem"
        Me.TrialToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.TrialToolStripMenuItem.Text = "Trial"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(279, 6)
        '
        'RemoveDupeDataToolStripMenuItem
        '
        Me.RemoveDupeDataToolStripMenuItem.Image = Global.SainsburysDashboardLoad.My.Resources.Resources.InvalidMerchTable_16__2_
        Me.RemoveDupeDataToolStripMenuItem.Name = "RemoveDupeDataToolStripMenuItem"
        Me.RemoveDupeDataToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.RemoveDupeDataToolStripMenuItem.Text = "Remove Dupe Data From DB"
        '
        'btnRun
        '
        Me.btnRun.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRun.Enabled = False
        Me.btnRun.Location = New System.Drawing.Point(624, 603)
        Me.btnRun.Margin = New System.Windows.Forms.Padding(4)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(100, 28)
        Me.btnRun.TabIndex = 3
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExit.Location = New System.Drawing.Point(732, 603)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(100, 28)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.labelStatus, Me.lblStatus, Me.tspb})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 645)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(848, 25)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'labelStatus
        '
        Me.labelStatus.Name = "labelStatus"
        Me.labelStatus.Size = New System.Drawing.Size(0, 20)
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(154, 20)
        Me.lblStatus.Text = "ToolStripStatusLabel1"
        '
        'tspb
        '
        Me.tspb.Name = "tspb"
        Me.tspb.Size = New System.Drawing.Size(220, 20)
        Me.tspb.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.tspb.Visible = False
        '
        'bgwRun
        '
        Me.bgwRun.WorkerReportsProgress = True
        '
        'bgwLoadFiles
        '
        '
        'btnDeDupe
        '
        Me.btnDeDupe.Location = New System.Drawing.Point(518, 603)
        Me.btnDeDupe.Name = "btnDeDupe"
        Me.btnDeDupe.Size = New System.Drawing.Size(90, 28)
        Me.btnDeDupe.TabIndex = 7
        Me.btnDeDupe.Text = "De-dupe"
        Me.btnDeDupe.UseVisualStyleBackColor = True
        '
        'AdditionalCountToolStripMenuItem
        '
        Me.AdditionalCountToolStripMenuItem.Image = CType(resources.GetObject("AdditionalCountToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AdditionalCountToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.AdditionalCountToolStripMenuItem.Name = "AdditionalCountToolStripMenuItem"
        Me.AdditionalCountToolStripMenuItem.Size = New System.Drawing.Size(282, 32)
        Me.AdditionalCountToolStripMenuItem.Text = "Additional Count"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(848, 670)
        Me.Controls.Add(Me.btnDeDupe)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.gbData)
        Me.Controls.Add(Me.gbDates)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(369, 706)
        Me.Name = "frmMain"
        Me.Text = "Dashboard Data Load"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.gbDates.ResumeLayout(False)
        Me.gbDates.PerformLayout()
        Me.gbData.ResumeLayout(False)
        Me.mnuExceptions.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbDates As System.Windows.Forms.GroupBox
    Friend WithEvents DatabaseConfigToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbData As System.Windows.Forms.GroupBox
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tspb As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents bgwRun As System.ComponentModel.BackgroundWorker
    Friend WithEvents lvData As System.Windows.Forms.ListView
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtEndDate As System.Windows.Forms.TextBox
    Friend WithEvents txtStartDate As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bgwLoadFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuExceptions As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearExceptionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents EmergencyCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents C1FullCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents C2CountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TACountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvalidSubCatCheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents RemoveDupeDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UtilitiesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BatchRebuildDataToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunWeeklyStoredProcedureToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunClothingStoredProcedureToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnDeDupe As System.Windows.Forms.Button
    Friend WithEvents labelStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TrialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdditionalCountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
