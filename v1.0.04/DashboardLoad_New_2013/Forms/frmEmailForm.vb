﻿Imports System.Configuration
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Outlook

Public Class frmEmailForm

    Private Sub EmailForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim sEmailList, sEmailDefault As String
        Dim sEmailArr As String()
        Dim nIndex As Integer

        sEmailList = System.Configuration.ConfigurationManager.AppSettings.Get("EmailTo")
        sEmailDefault = System.Configuration.ConfigurationManager.AppSettings.Get("EmailToDefault")
        sEmailArr = sEmailList.Split(",")
        cmbEmail.Items.AddRange(sEmailArr)
        nIndex = cmbEmail.FindString(sEmailDefault)
        cmbEmail.SelectedItem = cmbEmail.Items(nIndex)


    End Sub

    Public Sub SendEmail(sSubject As String, sBody As String, _
                         sFilename As String, sDisplayname As String)
        Dim oApp As Interop.Outlook._Application
        oApp = New Interop.Outlook.Application

        Dim oMsg As Interop.Outlook._MailItem
        oMsg = oApp.CreateItem(Interop.Outlook.OlItemType.olMailItem)

        oMsg.Subject = sSubject
        oMsg.Body = sBody

        oMsg.To = cmbEmail.Text
        oMsg.CC = ""

        Dim strS As String = sFilename
        Dim strN As String = sDisplayname
        If sFilename <> "" Then
            Dim sBodyLen As Integer = Int(sBody.Length)
            Dim oAttachs As Interop.Outlook.Attachments = oMsg.Attachments
            Dim oAttach As Interop.Outlook.Attachment

            oAttach = oAttachs.Add(strS, sBodyLen, strN)

        End If

        oMsg.Send()

    End Sub
End Class