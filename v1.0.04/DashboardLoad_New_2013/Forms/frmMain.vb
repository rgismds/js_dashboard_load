﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows
Imports Microsoft.Office

Public Class frmMain

#Region "Privates"
    Dim sProcessStartDate As String
    Dim sProcessEndDate As String
    Dim oLvItems As New List(Of ListViewItem)
    Dim oFilesToProcess As New List(Of String)
    Dim bErrors As Boolean = False
    Dim htExceptions As New Hashtable
#End Region

    Private Sub DatabaseConfigToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DatabaseConfigToolStripMenuItem.Click

        Try

            Dim frm As New frmConnection

            frm.udpConnection = oConn
            frm.udpTableName = "tbClientEnvironment"

            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                Call LoadForm()
            End If

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub OptionsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles OptionsToolStripMenuItem.Click

        Try

            Dim frm As New frmOptions
            frm.ShowDialog()

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Try

            If bgwLoadFiles.IsBusy OrElse bgwRun.IsBusy Then
                MsgBox("A process is running, please wait...", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                e.Cancel = True
            End If

        Catch ex As System.Exception

        End Try

    End Sub

    Private Sub frmMain_KeyUp(sender As Object, e As KeyEventArgs) Handles Me.KeyUp

        Try

            If e.KeyCode = Keys.C AndAlso e.Control Then

                Dim headers = (From ch In lvData.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

                Dim items = (From item In lvData.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems _
                            Let si = DirectCast(subitem, ListViewItem.ListViewSubItem) Select si.Text).ToArray()).ToArray()

                Dim table As String = String.Join(vbTab, headers) & Environment.NewLine

                For Each a In items
                    table &= String.Join(vbTab, a) & Environment.NewLine
                Next

                table = table.TrimEnd(CChar(Environment.NewLine))

                Clipboard.SetText(table)

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        System.Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
        Call LoadForm()
    End Sub

    Private Sub LoadForm()
        CenterToScreen()
        Try

            lblStatus.Text = ""

            Call SetupListView()
            ' Call SetupErrorLogs()
            Call LoadSavedConnectionDetailsFromRegistry()

            If CreateConnection(oConn.ConnectionString, oConn.Connection, oConn.Provider) Then

                'Close connection
                oConn.Connection.Close()

                '2014-08-14 SC
                'Choose Database
                '#2015# Call ChangeToSelectedDatabase()

                Dim dtTemp As DataTable = SqlQuery(oConn.ConnectionString, "SELECT CustomValue FROM Admin_Config WHERE CustomName LIKE 'FilterLV%'")

                If dtTemp IsNot Nothing AndAlso dtTemp.Rows.Count > 0 Then
                    For Each dRow As DataRow In dtTemp.Rows
                        oFilters.Add(dRow(0))
                    Next
                End If

                sDirectoryPath = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='DataLoadDir'", "")
                sStartTA = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_START_DATE_COL & "'", "")
                sEndTA = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='" & TA_END_DATE_COL & "'", "")

                gbDates.Enabled = True

                Call LoadExceptions()

                Call LoadStoreTypes()

            Else
                MsgBox("Invalid database configuration!", MsgBoxStyle.Exclamation)
                gbDates.Enabled = False
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            OptionsToolStripMenuItem.Enabled = gbDates.Enabled
            UtilitiesToolStripMenuItem.Enabled = gbData.Enabled
            gbData.Enabled = False
        End Try

    End Sub

    Private Sub ChangeToSelectedDatabase()

        Try

            Dim oFrm As New frmDatabase
            oFrm.StartPosition = FormStartPosition.CenterScreen
            oFrm.ShowDialog()

            If Not oFrm.bReturn Then
                btnLoad.Enabled = False
                gbDates.Enabled = False
                Throw New System.Exception("")
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadStoreTypes()

        Try

            dtStoreTypes = New DataTable
            dtStoreTypes = SqlQuery(oConn.ConnectionString, "SELECT Store_Number, Store_Type FROM TB_Retail_Hierarchy WHERE Count_Year IN (SELECT TOP(1) Count_Year FROM Financial_Calender ORDER BY Count_Year DESC)")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadExceptions()

        Try

            Dim dTable As DataTable = SqlQuery(oConn.ConnectionString, "SELECT Store_Number, Inventory_Date, ExclusionType FROM Inventory_Data_OUT15_Exclusions")

            If dTable IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                For Each dRow As DataRow In dTable.Rows

                    Dim oTag As New udtExceptionProps
                    oTag.iStoreNumber = dRow(0)
                    oTag.sInvDate = dRow(1)
                    oTag.sExclusionType = dRow(2)
                    If Not htExceptions.ContainsKey(dRow(0) & ":" & CDate(dRow(1)).ToString("yyyy-MM-dd")) Then
                        ' Add key if it doesnt exist alreadt in hash table
                        htExceptions.Add(dRow(0) & ":" & CDate(dRow(1)).ToString("yyyy-MM-dd"), oTag)
                    End If


                Next
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub SetupErrorLogs()

        Try

            With cLog

                .Reset()
                .p_Path = Forms.Application.StartupPath & "\Log_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml"

                'add Tables
                .p_Tables = "Files ¬ Errors ¬ NotImported ¬ ImportedFiles"

                'File Columns
                .p_Columns = "Files ¬ Name"

                'Errors Columns
                .p_Columns = "Errors ¬ Name ¬ LineError"

                'NotImported Columns
                .p_Columns = "NotImported ¬ StoreNumber ¬ Name ¬ Reason"

                'ImportedFiles Columns
                .p_Columns = "ImportedFiles ¬ StoreNumber ¬ Name"

                'Add relation FilesErrors between table Files and table Errors column = Name
                .AddRelation("FilesErrors", "Files", "Errors", "Name")

                'Add relation FilesNotImported between table Files and table NotImported column = Name
                .AddRelation("FilesNotImported", "Files", "NotImported", "Name")

                'Add relation FilesImportedFiles between table Files and table ImportedFiles column = Name
                .AddRelation("FilesImportedFiles", "Files", "ImportedFiles", "Name")

            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub SetupListView()

        Try

            lvData.Columns.Clear()

            Dim oColumn1, oColumn2, oColumn3, oColumn4, oColumn5, oColumn6, oColumn7 As New ColumnHeader

            oColumn1.Text = "Inv Date"
            oColumn1.Width = 64
            oColumn1.Tag = eColumnTag.eDate
            lvData.Columns.Add(oColumn1)

            oColumn2.Text = "Store No"
            oColumn2.Width = 55
            oColumn2.Tag = eColumnTag.eInteger
            lvData.Columns.Add(oColumn2)

            oColumn3.Text = "Store Type"
            oColumn3.Width = 68
            oColumn3.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn3)

            oColumn4.Text = "Exception"
            oColumn4.Width = 60
            oColumn4.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn4)

            oColumn5.Text = "Dest Table"
            oColumn5.Width = 165
            oColumn5.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn5)

            oColumn6.Text = "Duplicate"
            oColumn6.Width = 60
            oColumn6.Tag = eColumnTag.eString
            lvData.Columns.Add(oColumn6)

            oColumn7.Text = "Dupe Date"
            oColumn7.Width = 110
            oColumn7.Tag = eColumnTag.eDate
            lvData.Columns.Add(oColumn7)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub LoadSavedConnectionDetailsFromRegistry()

        Try
            Dim oIdb As IDbConnection = Nothing
            oConn = New clsConnection
            With oConn
                .ConnectionString = DecryptString(GetSetting(sKeyName, sSectionName, "ConnString", ""))
                .Database = DecryptString(GetSetting(sKeyName, sSectionName, "Database", ""))
                .Provider = DecryptString(GetSetting(sKeyName, sSectionName, "Provider", ""))
                .Pwd = DecryptString(GetSetting(sKeyName, sSectionName, "Pwd", ""))
                .User = DecryptString(GetSetting(sKeyName, sSectionName, "User", ""))
                .TrustedConnection = If(DecryptString(GetSetting(sKeyName, "Settings", "Trusted", "")) = "", False, DecryptString(GetSetting(sKeyName, sSectionName, "Trusted", "")))
                .Server = DecryptString(GetSetting(sKeyName, sSectionName, "Server", ""))
                .Connection = oIdb
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub txtStartDate_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtStartDate.TextChanged, txtEndDate.TextChanged

        Try

            btnLoad.Enabled = (txtStartDate.Text.Length > 0) AndAlso (txtEndDate.Text.Length > 0)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub dtpStart_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpStart.ValueChanged

        Try

            txtStartDate.Text = dtpStart.Value.ToString("yyyy-MM-dd")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub dtpEnd_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpEnd.ValueChanged

        Try

            txtEndDate.Text = dtpEnd.Value.ToString("yyyy-MM-dd")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub btnLoad_Click(sender As System.Object, e As System.EventArgs) Handles btnLoad.Click
        Call LoadFiles()
    End Sub

    Private Sub LoadFiles()

        Try

            If (sDirectoryPath.Length = 0) OrElse (Not IO.Directory.Exists(sDirectoryPath)) Then
                MsgBox("Data Load Directory path is not setup!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If oFilters.Count = 0 Then
                MsgBox("No filters have been setup!", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If txtStartDate.Text.Length = 0 OrElse txtEndDate.Text.Length = 0 Then
                MsgBox("Invalid Start/End date", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            'check start and end dates
            If dtpStart.Value.DayOfWeek <> DayOfWeek.Sunday OrElse dtpEnd.Value.DayOfWeek <> DayOfWeek.Saturday Then
                If MsgBox("Warning:" & vbNewLine & vbNewLine & _
                       "The start/end date is not expected Sunday/Saturday respectively." & vbNewLine & vbNewLine & _
                       "Do you want to continue?", MessageBoxIcon.Warning Or MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            lvData.Items.Clear()

            sProcessStartDate = Me.txtStartDate.Text
            sProcessEndDate = Me.txtEndDate.Text


            lblStatus.Text = "Loading data from files..."
            tspb.Visible = True
            gbDates.Enabled = False
            gbData.Enabled = False
            FileToolStripMenuItem.Enabled = False

            Windows.Forms.Cursor.Current = Cursors.WaitCursor

            bgwLoadFiles.RunWorkerAsync()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwLoadFiles_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadFiles.DoWork
        Call GetFiles(e)
    End Sub

    Private Sub GetFiles(e As System.ComponentModel.DoWorkEventArgs)

        Try

            oFilesToProcess.Clear()
            oLvItems.Clear()

            For Each oFilter In oFilters
                oFilesToProcess.AddRange(IO.Directory.GetFiles(sDirectoryPath, oFilter, IO.SearchOption.TopDirectoryOnly))
            Next

            Call ProcessFiles(oFilesToProcess, e)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ProcessFiles(oFiles As List(Of String), e As System.ComponentModel.DoWorkEventArgs)

        Try

            Dim iStoreNumberIndex As Integer = -1
            Dim sFileDate As String = ""
            Dim nFiles, nFilesProcessed As Integer

            nFiles = oFiles.Count
            nFilesProcessed = 0

            Cursor = Cursors.WaitCursor
          
            For Each oFile In oFiles

                If ShouldFileBeProcessed(oFile, iStoreNumberIndex, sFileDate) Then

                    Dim oStoreNumbers As SortedList = GetStoreNumbersInFile(oFile, iStoreNumberIndex, sFileDate)
                    Call AddDataToListview(oStoreNumbers)

                End If
                nFilesProcessed += 1
                tspb.Value = (nFilesProcessed / nFiles) * 100


            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
        lblStatus.Text = "Load completed"

        Cursor = Cursors.Default
    End Sub

    Private Function ShouldFileBeProcessed(sFile As String, ByRef iStoreIndex As Integer, ByRef sFileDate As String) As Boolean

        Dim bProcess = False
        'Dim sFileDate As String = ""

        Try

            If (IO.Path.GetFileNameWithoutExtension(sFile).ToUpper.StartsWith("PCVAR")) Then

                'PCVAR - get date from filename (PCVARddMMyyyy.csv)
                Dim sTemp As String = IO.Path.GetFileNameWithoutExtension(sFile).Replace("PCVAR", "")

                'sFileDate = sTemp.Substring(4, 4) & "-" & sTemp.Substring(2, 2) & "-" & sTemp.Substring(0, 2)
                sFileDate = CDate(sTemp.Substring(4, 4) & "-" & sTemp.Substring(2, 2) & "-" & sTemp.Substring(0, 2)).ToString("yyyy-MM-dd")
                bProcess = (sFileDate >= sProcessStartDate) AndAlso (sFileDate <= sProcessEndDate)
                iStoreIndex = ePCVarColumns.eStoreNumber

            Else
                '.Out15 - open file to get date
                Dim oRead As New IO.StreamReader(sFile)
                Dim oLine As String = oRead.ReadLine 'first line is always headers

                sFileDate = CDate(oLine.Split(",")(eOut16Columns.eInventoryDate).Replace("/", "-")).ToString("yyyy-MM-dd")
                bProcess = (sFileDate >= sProcessStartDate) AndAlso (sFileDate <= sProcessEndDate)
                iStoreIndex = eOut16Columns.eStoreNumber
                oRead.Close()

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            ShouldFileBeProcessed = bProcess
        End Try

    End Function

    Private Sub AddDataToListview(oList As SortedList)

        Try

            '0inventorydate
            '1storenumber
            '2storetype
            '3[blank] - exception field
            '4destination table - out15 / ta / N/A (not imported)
            '5Dupe detected

            For Each oDic As DictionaryEntry In oList

                Dim oTag As udtStoreProps = oDic.Value
                Dim sAdd(6) As String
                Dim sImpDate As String = ""

                sAdd(0) = oTag.sInvDate
                sAdd(1) = oTag.iStoreNumber
                sAdd(2) = oTag.sStoreType
                sAdd(3) = ""
                sAdd(4) = GetDestinationTable(oTag.sInvDate, IO.Path.GetExtension(oTag.sFullpath))
                sAdd(5) = If(Not DoesStoreNumberExistInDatabase(oTag.iStoreNumber, sAdd(4), oTag.sInvDate, sImpDate), "", "DUPE")
                sAdd(6) = sImpDate

                Dim lvItem As New ListViewItem(sAdd)
                lvItem.Tag = oTag

                If lvItem.SubItems(5).Text.Length > 0 Then lvItem.BackColor = Color.PaleVioletRed

                'exception already exist in db ?
                If (htExceptions.Contains(oTag.iStoreNumber & ":" & oTag.sInvDate)) Then
                    Call SetStatus(CType(htExceptions(oTag.iStoreNumber & ":" & oTag.sInvDate), udtExceptionProps).sExclusionType, lvItem)
                End If

                oLvItems.Add(lvItem)
                'lvData.Items.Add(lvItem)

            Next

            'lvData.Items.AddRange(oLvItems.ToArray)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function GetDestinationTable(sDate As String, sExt As String) As String

        Dim sTable As String = "Err"

        Try
            If sExt.Contains("csv") Then
                If (sDate >= sStartTA) AndAlso (sDate <= sEndTA) Then
                    sTable = eDestinationTables.Inventory_Data_TA_OUT15.ToString
                Else
                    sTable = eDestinationTables.Inventory_Data_OUT15.ToString
                End If
            Else
                If (sDate >= sStartTA) AndAlso (sDate <= sEndTA) Then
                    sTable = eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                Else
                    sTable = eDestinationTables.NOSF_Inventory_Data_OUT15.ToString
                End If
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetDestinationTable = sTable
        End Try

    End Function

    Private Function DoesStoreNumberExistInDatabase(iStoreNumber As Integer, sDestinationTable As String, sInvDate As String, ByRef sImpDate As String) As Boolean

        Dim bExists As Boolean = False

        Try
            If iStoreNumber = vbNull Then
                MessageBox.Show("No store number")
                Exit Try

            End If

            If iStoreNumber = 4275 Then
                iStoreNumber = iStoreNumber
            End If


            If sDestinationTable.Equals("[ignore]") Then Exit Try

            Dim sCurrentCY As String = ReadField("SELECT Count_Year FROM Financial_Calender WHERE [Inventory Date] ='" & sInvDate & "'").ToString.Trim
            Dim htTable As New Hashtable
            Dim sSQL As String = ""

            sSQL = "SELECT distinct(Count_Year), Financial_Calender.[Inventory Date] FROM " & sDestinationTable & " INNER JOIN Financial_Calender ON " & sDestinationTable & ".Inventory_Date = Financial_Calender.[Inventory Date] AND [Inventory Date] ='" & sInvDate & "' WHERE " & sDestinationTable & ".Store_Number='" & iStoreNumber & "'"
            Dim dTemp As DataTable = SqlQuery(oConn.ConnectionString, sSQL)

            If dTemp IsNot Nothing AndAlso dTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dTemp.Rows
                    htTable.Add(dRow(0).ToString.Trim, dRow(1).ToString.Trim)
                Next
            End If

            bExists = htTable.ContainsKey(sCurrentCY)

            If bExists Then
                sImpDate = CDate(htTable(sCurrentCY)).ToString("yyyy-MM-dd")
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            DoesStoreNumberExistInDatabase = bExists
        End Try

    End Function

    Private Function GetStoreNumbersInFile(sFile As String, iStoreIndex As Integer, sFileDate As String) As SortedList

        Dim oStoreNumbers As New SortedList
        Dim oLine As String = ""
        Dim oRead As New IO.StreamReader(sFile)
        Try

            'read file, get store num
            Dim sStoreNumber As String = ""
            Dim iIndex As Integer = 0

            oLine = oRead.ReadLine 'first line is always headers

            Do Until oLine Is Nothing

                If iStoreIndex = ePCVarColumns.eStoreNumber OrElse iIndex > 0 Then
                    oLine = oRead.ReadLine
                End If

                ' JR - 10/03/2017
                ' Check if No Data Be Reported message is in file, and if so then don't attempt to read any further
                If InStr(oLine, "*****No Data to be Reported*****") >= 1 Then Exit Do

                If oLine Is Nothing Then Exit Do

                sStoreNumber = oLine.Split(",")(iStoreIndex)

                'if not exists in ostorenumbers, add

                If sStoreNumber.Length = 0 Then Exit Do

                If (Not oStoreNumbers.Contains(sStoreNumber)) Then
                    Dim oTag As New udtStoreProps
                    oTag.iStoreNumber = sStoreNumber
                    oTag.sFilename = IO.Path.GetFileName(sFile)
                    oTag.sFullpath = sFile
                    oTag.sInvDate = sFileDate

                    ' JR - 19/07/2017
                    ' Check if store number is in array populated from TB_Retail_Hierarchy first
                    ' and if not then warn user
                    Dim tbrhRow() As DataRow = dtStoreTypes.Select("Store_Number=" & oTag.iStoreNumber)
                    If tbrhRow.Count = 0 Then
                        MsgBox("Store number " & oTag.iStoreNumber & " not found in TB_Retail_Hierarchy table. Store type cannot be obtained.", MsgBoxStyle.Exclamation)

                    Else
                        oTag.sStoreType = dtStoreTypes.Select("Store_Number=" & oTag.iStoreNumber)(0)("Store_Type")
                    End If
                    oStoreNumbers.Add(sStoreNumber, oTag)
                End If
                    iIndex = 1
            Loop

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            oRead.Close()
            GetStoreNumbersInFile = oStoreNumbers
        End Try

    End Function

    Private Sub bgwLoadFiles_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadFiles.RunWorkerCompleted

        Try

            lblStatus.Text = "Finished loading data."
            tspb.Visible = False

            gbDates.Enabled = True
            gbData.Enabled = True
            FileToolStripMenuItem.Enabled = True

            lvData.Items.Clear()
            lvData.Items.AddRange(oLvItems.ToArray)

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            If lvData.Items.Count > 0 Then
                lvData.Items(0).Selected = True
                btnDeDupe.Enabled = DuplicatesExist()
            End If
            Windows.Forms.Cursor.Current = Cursors.Default
        End Try

    End Sub

    Private Sub TestToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        MsgBox(lvData.Columns(0).Width)
    End Sub

    Private Sub btnExit_Click(sender As System.Object, e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnRun_Click(sender As System.Object, e As System.EventArgs) Handles btnRun.Click
        Call RunProcess()
    End Sub

    Private Sub RunProcess()

        Try

            'If "DUPE" exists then we shall not continue! - Shane Cawser 2013-10-14
            If Not DuplicatesExist() Then

                tspb.Visible = True
                lblStatus.Text = "Testing Data Integrity..."

                gbDates.Enabled = False
                gbData.Enabled = False
                FileToolStripMenuItem.Enabled = False
                btnRun.Enabled = False

                dtTableOut15 = CreateDataTable()
                dtTableTaData = CreateDataTable()
                dtTableNOSF = CreateDataTable()
                dtTableNOSFTa = CreateDataTable()

                Windows.Forms.Cursor.Current = Cursors.WaitCursor

                Call SetupErrorLogs()

                bgwRun.RunWorkerAsync()

            Else

                MsgBox("Please resolve duplicates before proceeding with data import.", MsgBoxStyle.Critical)

            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwRun_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwRun.DoWork
        Call InsertData(e)
    End Sub

    Private Function DuplicatesExist() As Boolean

        Dim oDupes As IEnumerable(Of ListViewItem) = Nothing

        Try
            oDupes = From lvItem In lvData.Items.Cast(Of ListViewItem)() Where lvItem.SubItems(eListviewColumns.eDupe).Text = "DUPE" And lvItem.SubItems(eListviewColumns.eException).Text <> "TRIAL" And lvItem.SubItems(eListviewColumns.eException).Text <> "ADD"

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            DuplicatesExist = (oDupes IsNot Nothing) AndAlso (oDupes.Count > 0)
        End Try

    End Function

    Private Sub InsertData(e As System.ComponentModel.DoWorkEventArgs)
        Dim nItemsToProcess As Integer, nItemsProcessed As Integer
        Dim nRgisCount, nOrridgeCount As Integer
        Dim sMessage, sCaption As String
        Dim log_file As StreamWriter
        Dim logFilename, sBody, sFinishTime As String



        Try
            Dim slTemp As New SortedList
            lblStatus.Text = "Import preparation progress"
            tspb.Value = 0
            Cursor = Cursors.WaitCursor
            nItemsToProcess = oLvItems.Count
            For Each lvItem In oLvItems

                If Not slTemp.Contains(CType(lvItem.Tag, udtStoreProps).sFilename) Then
                    cLog.p_Message = "Files ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename
                    slTemp.Add(CType(lvItem.Tag, udtStoreProps).sFilename, CType(lvItem.Tag, udtStoreProps))
                End If

                Select Case lvItem.SubItems(eListviewColumns.eException).Text

                    Case ""
                        'No exception, import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "TRIAL"
                        ' Trial data , import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "ADD"
                        ' Additional count data , import as normal
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename

                    Case "EM"
                        'Emergency Count, do not import - Log
                        cLog.p_Message = "NotImported ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Emergency Count"
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "EM")

                    Case "TA"
                        'TA Count outside or TA date range - TA Table
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "TA")
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Enforced - TA Exception"

                    Case "C1/FU"
                        'C1/Full Count inside TA date range - Out15 Table
                        Call ImportFileToDataTable(lvItem.SubItems(eListviewColumns.eTable).Text, lvItem.Tag, lvItem.SubItems(eListviewColumns.eException).Text)
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "C1/FU")
                        cLog.p_Message = "ImportedFiles ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ Enforced - C1/FU Exception"

                    Case "C2"
                        'C2 count, do not import - Log
                        cLog.p_Message = "NotImported ¬ " & CType(lvItem.Tag, udtStoreProps).iStoreNumber & " ¬ " & CType(lvItem.Tag, udtStoreProps).sFilename & " ¬ C2 Count"
                        Call UpdateExceptionInDatabase(CType(lvItem.Tag, udtStoreProps), "C2")

                End Select

                If bErrors Then
                    MsgBox("Errors have been found in file: " & CType(lvItem.Tag, udtStoreProps).sFilename & ". Check Log File!", MsgBoxStyle.Critical)
                    Exit Try
                End If
                nItemsProcessed += 1
                tspb.Value = (nItemsProcessed / nItemsToProcess) * 100
            Next

            If (My.Application.CommandLineArgs.Count = 1) AndAlso (My.Application.CommandLineArgs(0).ToString = "Debug=1") Then Call DumpDataTable()

            Call BulkImportToSQL()
            Call StampProcessDates()

            '2014-08-12 SC
            '#2015# Call ImportClothingData()
            '#2015# Call ImportSuperCatVerifier()

            '2014-09-04 SC
            '#2015# Call ImportPIQA()
            Cursor = Cursors.Default
            If MsgBox("Import of Data Complete!" & vbNewLine & vbNewLine & _
                      "Do you want to run Stored Procedures now ?", MsgBoxStyle.Information + MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                logFilename = "Dashboard_Load_" + Date.Now.ToString("ddMMMyyyy") + ".txt"

                log_file = New StreamWriter(logFilename, False)
                sFinishTime = DateTime.Now.ToShortTimeString()
                log_file.WriteLine("Started Stored Procedure run for Dashboard Load: {0} {1}", sFinishTime, _
                                DateTime.Now.ToShortDateString())


                Call RunStoredProcedure("SAINSBURY_DASHBOARD_VIEWDATA")
                
                ' JR - 23/08/2017
                ' Added message box to display the number of RGIS stores and Orridge stores that were uploaded
                Call GetInventoryCompanyStoreCounts(nRgisCount, nOrridgeCount)
                sCaption = "Sainsburys Dashboard Load - " + DateTime.Now.Date.ToString("dd/MM/yyyy")
                sMessage = "A total of " + nRgisCount.ToString() + " RGIS stores, "
                sMessage += "and " + nOrridgeCount.ToString() + " Orridge stores have been uploaded."
                log_file.WriteLine(sMessage)
                log_file.WriteLine("Finished Stored Procedure run for Dashboard Load: {0} {1}", DateTime.Now.ToShortTimeString(), _
                DateTime.Now.ToShortDateString())
                log_file.Close()

                Call MessageBox.Show(sMessage, sCaption, MessageBoxButtons.OK, MessageBoxIcon.Information)
                If frmEmailForm.ShowDialog() Then
                    ' JR - 07/09/2017
                    ' Added Email dialog to send on count of RGIS stores and Orridge stores that were uploaded
                    sBody = "Please note the Sainsburys Dashboard data load completed at " + sFinishTime + " on " + DateTime.Now.Date.ToString("dd/MM/yyyy")
                    sBody += vbCrLf + vbCrLf + sMessage
                    Call frmEmailForm.SendEmail("Sainsburys Dashboard Load - " + Date.Today.ToShortDateString(), sBody, "", "")
                End If
            End If
            Call RunInvalidSubCatReport()
            Call RunLoadExceptionsReport()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            cLog.WriteToXml()
        End Try

    End Sub



    Private Sub ImportPIQA()
        Call PiqaImport()
    End Sub

    Private Sub RunLoadExceptionsReport()

        Try

            Dim oFrm As New frmLoadExceptions
            oFrm.StartDate = sProcessStartDate
            oFrm.EndDate = sProcessEndDate

            oFrm.ShowDialog()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub RunInvalidSubCatReport()

        Try
            tspb.Value = 0
            lblStatus.Text = "Running invalid sub cat report"
            Cursor = Cursors.WaitCursor
            sProcessStartDate = If(sProcessStartDate.Length = 0, "", sProcessStartDate)
            sProcessEndDate = If(sProcessEndDate.Length = 0, "", sProcessEndDate)

            Dim sSQL As String = "exec SP_Check_for_Invalid_Subcats " & If(sProcessStartDate.Length = 0 AndAlso sProcessEndDate.Length = 0, "", "'" & sProcessStartDate & "','" & sProcessEndDate & "'")
            Dim dTable As DataTable = SqlQuery(oConn.ConnectionString, sSQL)
            dTable.TableName = "TB_Load_Exceptions"
            Cursor = Cursors.Default
            If dTable IsNot Nothing AndAlso dTable.Rows.Count > 0 Then
                dTable.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\Invalid_SubCats_" & Now.ToString("yyyy_MM_dd_HH_mm_ss") & ".xml")
                MsgBox("One or more invalid sub cats has been detected. File exported to desktop", MsgBoxStyle.Information)
            Else
                MsgBox("No invalid sub cats detected.", MsgBoxStyle.Information)
            End If
            lblStatus.Text = ""
        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
    Private Sub GetInventoryCompanyStoreCounts(ByRef rgisCount As Integer, ByRef orridgeCount As Integer)

        Dim xoCmd As SqlCommand

        xoCmd = New SqlCommand()

        xoCmd.Connection = oConn.Connection
        xoCmd.CommandType = System.Data.CommandType.StoredProcedure

        xoCmd.Connection.Open()
        xoCmd.CommandText = "dbo.p_GetStoreUploadCounts"
        xoCmd.Parameters.Add("Date_Created", SqlDbType.Date).Value = DateTime.Now.Date
        xoCmd.Parameters.Add("Rgis_Store_Count", SqlDbType.Int, 10).Direction = ParameterDirection.Output
        xoCmd.Parameters.Add("Orridge_Store_Count", SqlDbType.Int, 10).Direction = ParameterDirection.Output
        xoCmd.ExecuteNonQuery()
        xoCmd.Connection.Close()
        orridgeCount = xoCmd.Parameters("Orridge_Store_Count").Value
        rgisCount = xoCmd.Parameters("Rgis_Store_Count").Value

    End Sub

    Private Sub RunStoredProcedure(sSP As String)

        Dim sQuery As String = "exec " & sSP

        Try
            Cursor = Cursors.WaitCursor
            tspb.Value = 0
            bgwRun.ReportProgress(0, "Running Stored Procedures...")
            Call executeQuery(oConn.ConnectionString, sSP)
            bgwRun.ReportProgress(0, "Stored Procedures Finished!")
            Cursor = Cursors.Default

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub StampProcessDates()

        Try

            Dim sSQL As String = "DELETE FROM Process_Dates"
            Call executeQuery(oConn.ConnectionString, sSQL)

            Dim oInsert As New clsInsert
            With oInsert
                .Reset()
                .TableName("Process_Dates")
                .BuildInsert("Process_Start_Date", sProcessStartDate)
                .BuildInsert("Process_End_Date", sProcessEndDate)
                Call executeQuery(oConn.ConnectionString, .GetSql)
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub UpdateExceptionInDatabase(oStoreProps As udtStoreProps, sException As String)

        Try

            Dim sSQL As String = "DELETE FROM Inventory_Data_OUT15_Exclusions WHERE Inventory_Date='" & CDate(oStoreProps.sInvDate).ToString("yyyy-MM-dd") & "' AND Store_Number='" & oStoreProps.iStoreNumber & "'"
            Call executeQuery(oConn.ConnectionString, sSQL)

            Dim oInsert As New clsInsert
            With oInsert
                .Reset()
                .TableName("Inventory_Data_OUT15_Exclusions")
                .BuildInsert("Store_Number", oStoreProps.iStoreNumber)
                .BuildInsert("Inventory_Date", CDate(oStoreProps.sInvDate).ToString("yyyy-MM-dd"))
                .BuildInsert("ExclusionType", sException)
                Call executeQuery(oConn.ConnectionString, .GetSql)
            End With

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub BulkImportToSQL()

        Try

            If (dtTableOut15.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing Out15 Data...")
                Call ImportDataTableToDatabase(dtTableOut15, "Inventory_Data_Out15")
            End If

            If (dtTableTaData.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing TA Data...")
                Call ImportDataTableToDatabase(dtTableTaData, "Inventory_Data_TA_Out15")
            End If

            If (dtTableNOSF.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing NOSF Data...")
                Call ImportDataTableToDatabase(dtTableNOSF, "NOSF_Inventory_Data_OUT15")
            End If

            If (dtTableNOSFTa.Rows.Count > 0) Then
                bgwRun.ReportProgress(0, "Importing NOSF TA Data...")
                Call ImportDataTableToDatabase(dtTableNOSFTa, "NOSF_Inventory_Data_TA_OUT15")
            End If


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ImportDataTableToDatabase(dt As DataTable, sDestinationTable As String)

        Try

            Using cn As New SqlConnection(oConn.ConnectionString)
                cn.Open()
                Using copy As New SqlBulkCopy(cn)

                    For i = 0 To dt.Columns.Count - 1
                        copy.ColumnMappings.Add(dt.Columns(i).ColumnName, dt.Columns(i).ColumnName)
                    Next

                    copy.BulkCopyTimeout = 0
                    copy.DestinationTableName = sDestinationTable
                    copy.WriteToServer(dt)

                End Using
            End Using


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub DumpDataTable()

        Try

            If dtTableOut15.Rows.Count > 0 Then dtTableOut15.WriteXml(Forms.Application.StartupPath & "\DataInserted_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableTaData.Rows.Count > 0 Then dtTableTaData.WriteXml(Forms.Application.StartupPath & "\DataInserted_TA_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableNOSF.Rows.Count > 0 Then dtTableNOSF.WriteXml(Forms.Application.StartupPath & "\DataInserted_NOSF_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")
            If dtTableNOSFTa.Rows.Count > 0 Then dtTableNOSFTa.WriteXml(Forms.Application.StartupPath & "\DataInserted_NOSF_TA_Out15_" & Now.ToString("yyyy_MM_dd hh_mm_ss") & ".xml")

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ImportFileToDataTable(sTableName As String, oTag As udtStoreProps, sException As String)

        Dim oRead As New IO.StreamReader(oTag.sFullpath, True)

        Try

            Dim dTable As DataTable = GetDataTableForImport(sTableName)
            Dim oLine As String = ""
            Dim iMaxColCount As Integer = -1
            Dim iType As eType = -1
            Dim bOK As Boolean = False
            Dim nTrial As Integer
            Dim nAdditionalCount As Integer

            nTrial = vbNull

            If (IO.Path.GetFileNameWithoutExtension(oTag.sFullpath).ToUpper.StartsWith("PCVAR")) Then
                'Get first line - to be ignored
                oLine = oRead.ReadLine
                'set max columns - if item desc contains "," it screws up array
                iMaxColCount = 11
                iType = eType.ePCVAR
            Else
                'set max columns for .out15 - if item desc contains "," it screws up array
                iMaxColCount = 12
                iType = eType.eOut16
            End If

            While Not oRead.EndOfStream

                Dim sData() As String

                oLine = oRead.ReadLine
                sData = oLine.Split(",")

                Try

                    If sData.GetUpperBound(0) <> iMaxColCount Then
                        'incorrect array - productdesc contains a "," - fix it!
                        sData = ExcelCommaFix(sData, iMaxColCount, iType)
                    End If

                    If (iType = eType.ePCVAR) Then
                        bOK = (oTag.iStoreNumber = sData(ePCVarColumns.eStoreNumber))
                    ElseIf iType = eType.eOut16 Then
                        bOK = (oTag.iStoreNumber = sData(eOut16Columns.eStoreNumber))
                    End If

                    nTrial = Nothing
                    nAdditionalCount = Nothing

                    If bOK Then
                        'correct array now, try importing
                        ' JR - 05/07/2017
                        ' Added flag to indicate if data is trial data
                        ' JR - 01/08/2017
                        ' Added flag to indicate if data is additional count data
                        If sException = "TRIAL" Then
                            nTrial = 1
                        ElseIf sException = "ADD" Then
                            nAdditionalCount = 1
                        End If
                        If iType = eType.ePCVAR Then
                            dTable.Rows.Add(CDate(oTag.sInvDate).ToString("yyyy-MM-dd"), sData(ePCVarColumns.eStoreNumber), 1, 1, 1, sData(ePCVarColumns.eSKU), sData(ePCVarColumns.eItemDesc), sData(ePCVarColumns.eRetailPrice), If(sData(ePCVarColumns.eSuperCat).Length = 0, DBNull.Value, sData(ePCVarColumns.eSuperCat)), If(sData(ePCVarColumns.eSubCat).Length = 0, DBNull.Value, sData(ePCVarColumns.eSubCat)), sData(ePCVarColumns.eOnHand), sData(ePCVarColumns.eActualQty), 0, False, nTrial, nAdditionalCount)
                        ElseIf iType = eType.eOut16 Then
                            dTable.Rows.Add(CDate(oTag.sInvDate).ToString("yyyy-MM-dd"), sData(eOut16Columns.eStoreNumber), 1, 1, 1, sData(eOut16Columns.eSKU), sData(eOut16Columns.eItemDesc), sData(eOut16Columns.eRetailPrice), If(sData(eOut16Columns.eSuperCat).Length = 0, DBNull.Value, sData(eOut16Columns.eSuperCat)), If(sData(eOut16Columns.eSubCat).Length = 0, DBNull.Value, sData(eOut16Columns.eSubCat)), sData(eOut16Columns.eOnHand), sData(eOut16Columns.eActualQty), sData(eOut16Columns.eBackroom), False, nTrial, nAdditionalCount)
                        End If

                    End If

                Catch ex As System.Exception
                    cLog.p_Message = "Errors ¬ " & oTag.sFilename & " ¬ Err Msg: " & ex.Message & " Line (" & oLine & ")"
                    bErrors = True
                End Try

            End While

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            Try
                oRead.Close()
                oRead.Dispose()
            Catch ex As System.Exception
                MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
            End Try

        End Try

    End Sub

    Private Function GetDataTableForImport(sTable As String) As DataTable

        Dim dTable As DataTable = dtTableOut15

        Try

            Select Case sTable
                Case eDestinationTables.Inventory_Data_OUT15.ToString
                    dTable = dtTableOut15
                Case eDestinationTables.Inventory_Data_TA_OUT15.ToString
                    dTable = dtTableTaData
                Case eDestinationTables.NOSF_Inventory_Data_OUT15.ToString
                    dTable = dtTableNOSF
                Case eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                    dTable = dtTableNOSFTa
            End Select

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetDataTableForImport = dTable
        End Try

    End Function

    Private Sub bgwRun_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwRun.ProgressChanged

        Try

            lblStatus.Text = e.UserState.ToString

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub bgwRun_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwRun.RunWorkerCompleted

        Try

            tspb.Visible = False
            lblStatus.Text = ""

            gbDates.Enabled = True
            gbData.Enabled = True
            FileToolStripMenuItem.Enabled = True
            btnRun.Enabled = True

            For Each lvItem As ListViewItem In lvData.Items
                Call ChangeListviewItemText(lvItem, lvItem.SubItems(eListviewColumns.eException).Text)
            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            Windows.Forms.Cursor.Current = Cursors.Default
            MsgBox("Completed!", MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub mnuExceptions_Opening(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles mnuExceptions.Opening

        Try

            e.Cancel = lvData.SelectedItems.Count = 0
            RemoveDupeDataToolStripMenuItem.Enabled = lvData.SelectedItems(0).SubItems(eListviewColumns.eDupe).Text.Length > 0

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Sub ClearExceptionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ClearExceptionToolStripMenuItem.Click
        Call SetStatus("")
    End Sub

    Private Sub EmergencyCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EmergencyCountToolStripMenuItem.Click
        Call SetStatus("EM")
    End Sub

    Private Sub AdditionalCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AdditionalCountToolStripMenuItem.Click
        Call SetStatus("ADD")
    End Sub


    Private Sub SetStatus(sStatus As String, Optional ByRef oItem As ListViewItem = Nothing)

        Try
            Dim sNewTable As String = ""

            If oItem IsNot Nothing Then
                oItem = ChangeListviewItemText(oItem, sStatus)
            Else
                For Each lvItem As ListViewItem In lvData.SelectedItems
                    lvItem = ChangeListviewItemText(lvItem, sStatus)
                Next
            End If

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    Private Function ChangeListviewItemText(ByRef lvItem As ListViewItem, ByVal sStatus As String) As ListViewItem

        Dim oItem As ListViewItem = lvItem

        Try
            Dim sNewTable As String = ""
            Dim sImpDate As String = ""

            Select Case sStatus
                Case ""
                    sNewTable = GetDestinationTable(oItem.SubItems(0).Text, IO.Path.GetExtension(CType(oItem.Tag, udtStoreProps).sFullpath))
                Case "EM"
                    sNewTable = "[ignore]"
                Case "C1/FU"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
                Case "C2"
                    sNewTable = "[ignore]"
                Case "TA"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_TA_OUT15", "NOSF_Inventory_Data_TA_OUT15")
                Case "TRIAL"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
                Case "ADD"
                    sNewTable = If(CType(oItem.Tag, udtStoreProps).sFullpath.EndsWith(".csv"), "Inventory_Data_OUT15", "NOSF_Inventory_Data_OUT15")
            End Select

            oItem.SubItems(eListviewColumns.eException).Text = sStatus
            oItem.SubItems(eListviewColumns.eTable).Text = sNewTable
            oItem.SubItems(eListviewColumns.eDupe).Text = If(Not DoesStoreNumberExistInDatabase(oItem.SubItems(eListviewColumns.eStoreNo).Text, sNewTable, oItem.SubItems(eListviewColumns.eInvDat).Text, sImpDate), "", "DUPE")
            oItem.SubItems(eListviewColumns.eImpDate).Text = sImpDate

            If oItem.SubItems(eListviewColumns.eDupe).Text.Length > 0 Then
                lvItem.BackColor = Color.PaleVioletRed
            Else
                lvItem.BackColor = Color.White
            End If


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            ChangeListviewItemText = oItem
        End Try

    End Function

    Private Sub C1FullCountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles C1FullCountToolStripMenuItem.Click
        Call SetStatus("C1/FU")
    End Sub

    Private Sub C2CountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles C2CountToolStripMenuItem.Click
        Call SetStatus("C2")
    End Sub

    Private Sub TACountToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TACountToolStripMenuItem.Click
        Call SetStatus("TA")
    End Sub
    Private Sub TrialToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TrialToolStripMenuItem.Click
        Call SetStatus("TRIAL")
    End Sub

    Private Sub lvData_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles lvData.ColumnClick
        Call SortListview(lvData, e.Column)
    End Sub

    Private Sub lvData_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lvData.SelectedIndexChanged
        btnRun.Enabled = lvData.Items.Count > 0
    End Sub

    Private Sub SortListview(ByVal lv As ListView, ByVal iColIndex As Integer)
        Try
            Dim col As ColumnHeader = lv.Columns(iColIndex)
            Dim sort_order As System.Windows.Forms.SortOrder
            Dim iType As Integer = 0

            sort_order = lv.Tag
            Select Case sort_order
                Case Windows.Forms.SortOrder.Ascending
                    sort_order = Windows.Forms.SortOrder.Descending
                Case Windows.Forms.SortOrder.Descending
                    sort_order = Windows.Forms.SortOrder.Ascending
                Case Windows.Forms.SortOrder.None
                    sort_order = Windows.Forms.SortOrder.Ascending
            End Select

            lv.Tag = sort_order

            'Get the type
            iType = lv.Columns(iColIndex).Tag

            'Set the new sort options
            lv.ListViewItemSorter = New ListViewColumnSorter(iColIndex, sort_order, iType)

            ' Perform the sort with these new sort options.
            lv.Sort()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub InvalidSubCatCheckToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles InvalidSubCatCheckToolStripMenuItem.Click
        Call RunInvalidSubCatReport()
    End Sub

    Private Sub FileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FileToolStripMenuItem.Click
        InvalidSubCatCheckToolStripMenuItem.Enabled = txtStartDate.Text.Length > 0 AndAlso txtEndDate.Text.Length > 0
    End Sub

    Private Sub RemoveDupeDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveDupeDataToolStripMenuItem.Click
        Call RemoveDupeFromDatabase()
    End Sub

    Private Sub RemoveDupeFromDatabase()
        Cursor = Cursors.WaitCursor
        Try

            Dim sSurveyType As String = GetSurveyType(lvData.SelectedItems(0).SubItems(eListviewColumns.eTable).Text.Trim)
            Dim sStore_Number As String = lvData.SelectedItems(0).SubItems(eListviewColumns.eStoreNo).Text.Trim
            Dim sInventoryDate As String = lvData.SelectedItems(0).SubItems(eListviewColumns.eImpDate).Text.Trim
            Dim sSQL As String = ""

            sSQL = "EXEC p_RemoveSurveyData " &
                   "@Survey_Type = N'" & sSurveyType & "', " &
                   "@Store_Number = " & sStore_Number & ", " &
                   "@Inventory_Date = N'" & sInventoryDate & "'"

            Call executeQuery(oConn.ConnectionString, sSQL)

            'Force Refresh all listview items that have same store number
            For Each lvItem As ListViewItem In lvData.Items
                If lvItem.SubItems(eListviewColumns.eStoreNo).Text = sStore_Number Then
                    Call SetStatus("", lvItem)
                End If
            Next

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)


        End Try
        Cursor = Cursors.Default
    End Sub

    Private Sub RemoveAllDupesFromDatabase()

        Dim oDupes As IEnumerable(Of ListViewItem) = Nothing
        Dim nDupes, nDupesRemoved As Integer
        Dim ret As Integer
        Dim sSurveyType As String
        Dim nStore_Number As Integer
        Dim sStore_Number As String
        Dim dtInventoryDate As Date
        Dim cmd As SqlClient.SqlCommand
        Dim listStoreNums As List(Of Integer)

        Cursor = Cursors.WaitCursor
        tspb.Visible = True
        tspb.Value = 0
        lblStatus.Text = "Duplicates Removal Progress"
        StatusStrip1.Refresh()


        Try
            listStoreNums = New List(Of Integer)
            oDupes = From lvItem In lvData.Items.Cast(Of ListViewItem)() Where lvItem.SubItems(eListviewColumns.eDupe).Text = "DUPE"
            nDupes = oDupes.Count
            nDupesRemoved = 0
            cmd = New SqlClient.SqlCommand("p_RemoveSurveyData", oConn.Connection)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@Survey_Type", SqlDbType.NVarChar)
            cmd.Parameters.Add("@Store_Number", SqlDbType.Int)
            cmd.Parameters.Add("@Inventory_Date", SqlDbType.DateTime)
            cmd.Connection.Open()

            For Each lvDupeItem As ListViewItem In oDupes
                sSurveyType = GetSurveyType(lvDupeItem.SubItems(eListviewColumns.eTable).Text.Trim)
                sStore_Number = lvDupeItem.SubItems(eListviewColumns.eStoreNo).Text.Trim
                nStore_Number = CInt(sStore_Number)
                dtInventoryDate = CDate(lvDupeItem.SubItems(eListviewColumns.eImpDate).Text.Trim)
                'Dim sSQL As String = ""

                ' sSQL = "EXEC p_RemoveSurveyData " &
                '       "@Survey_Type = N'" & sSurveyType & "', " &
                '       "@Store_Number = " & sStore_Number & ", " &
                '       "@Inventory_Date = N'" & sInventoryDate & "'"

                ' Call executeQuery(oConn.ConnectionString, sSQL)

                cmd.Parameters("@Survey_Type").Value = sSurveyType
                cmd.Parameters("@Store_Number").Value = nStore_Number
                cmd.Parameters("@Inventory_Date").Value = dtInventoryDate

                ret = cmd.ExecuteNonQuery

                Call SetStatus("", lvDupeItem)

                If listStoreNums.BinarySearch(sStore_Number) <= -1 Then
                    ' JR - 10/03/2017
                    ' Add number to list as after duplicates are removed from database the corresponding items in the list will need status reset
                    Call listStoreNums.Add(sStore_Number)
                End If


                ' Update progress bar to record duplicate as being removed
                nDupesRemoved += 1
                tspb.Value = (nDupesRemoved / nDupes) * 100
            Next
            'Force Refresh all listview items that have same store number

            For Each lvItem As ListViewItem In lvData.Items
                lvItem.Selected = True
                lvItem.Focused = True
                sStore_Number = lvItem.SubItems(eListviewColumns.eStoreNo).Text
                If listStoreNums.BinarySearch(sStore_Number) >= 0 Then
                    Call SetStatus("", lvItem)
                    Call lvData.Update()
                    Call lvData.Refresh()
                End If
            Next


        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)

        Finally
            cmd.Connection.Close()
        End Try

        Cursor = Cursors.Default
        tspb.Visible = False
        Call MsgBox("A total of " + nDupesRemoved.ToString() + " were removed from the database.", MsgBoxStyle.Information)
        lblStatus.Text = ""
        btnDeDupe.Enabled = False

    End Sub

    Private Function GetSurveyType(ByVal sTablename As String) As String

        Dim sSurveyType As String = "Full"

        Try

            Select Case sTablename
                Case eDestinationTables.Inventory_Data_TA_OUT15.ToString, eDestinationTables.NOSF_Inventory_Data_TA_OUT15.ToString
                    sSurveyType = "TA"
            End Select

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        Finally
            GetSurveyType = sSurveyType
        End Try

    End Function

    Private Sub BatchRebuildDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatchRebuildDataToolStripMenuItem.Click
        Try

            Dim frm As New frmBatchBuild
            frm.ShowDialog()

        Catch ex As System.Exception
            MsgBox(Reflection.MethodBase.GetCurrentMethod().Name & ": " & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub RunWeeklyStoredProcedureToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunWeeklyStoredProcedureToolStripMenuItem.Click
        Call RunStoredProcedure("SAINSBURY_DASHBOARD_VIEWDATA")
    End Sub

    Private Sub RunClothingStoredProcedureToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunClothingStoredProcedureToolStripMenuItem.Click
        Call RunStoredProcedure("usp_Clothing_Data_Build")
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Call Forms.Application.Exit()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnDeDupe.Click
        Call RemoveAllDupesFromDatabase()
    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs)

    End Sub
End Class
