﻿Public Class RptCol

    Enum ColDataTypes
        DTString = 1
        DTInteger = 2
        DTDecimal = 3
        DTBoolean = 4
    End Enum


    Private m_ColName As String
    Public Property ColName() As String
        Get
            Return m_ColName
        End Get
        Set(ByVal value As String)
            m_ColName = value
        End Set
    End Property

    Private m_ColStartInput As Integer
    Public Property ColStartInput() As Integer
        Get
            Return m_ColStartInput
        End Get
        Set(ByVal value As Integer)
            m_ColStartInput = value
        End Set
    End Property

    Private m_ColEndInput As Integer
    Public Property ColEndInput() As Integer
        Get
            Return m_ColEndInput
        End Get
        Set(ByVal value As Integer)
            m_ColEndInput = value
        End Set
    End Property

    Private m_ColStartOutput As Integer
    Public Property ColStartOutput() As Integer
        Get
            Return m_ColStartOutput
        End Get
        Set(ByVal value As Integer)
            m_ColStartOutput = value
        End Set
    End Property

    Private m_ColEndOutput As Integer
    Public Property ColEndOutput() As Integer
        Get
            Return m_ColEndOutput
        End Get
        Set(ByVal value As Integer)
            m_ColEndOutput = value
        End Set
    End Property

    Private m_ColDataType As ColDataTypes
    Public Property ColDataType() As ColDataTypes
        Get
            Return m_ColDataType
        End Get
        Set(ByVal value As ColDataTypes)
            m_ColDataType = value
        End Set
    End Property

    Private m_ColDecimals As Integer
    Public Property ColDecimals() As Integer
        Get
            Return m_ColDecimals
        End Get
        Set(ByVal value As Integer)
            m_ColDecimals = value
        End Set
    End Property

    Private m_ColHdgInput As String
    Public Property ColHdgInput() As String
        Get
            Return m_ColHdgInput
        End Get
        Set(ByVal value As String)
            m_ColHdgInput = value
        End Set
    End Property

    Private m_ColHdgForOutput As String
    Public Property ColHdgForOutput() As String
        Get
            Return m_ColHdgForOutput
        End Get
        Set(ByVal value As String)
            m_ColHdgForOutput = value
        End Set
    End Property

    Private m_ColStringValue As String
    Public Property ColStringValue() As String
        Get
            Return m_ColStringValue
        End Get
        Set(ByVal value As String)
            m_ColStringValue = value
        End Set
    End Property

End Class
