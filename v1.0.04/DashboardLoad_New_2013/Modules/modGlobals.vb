﻿Module modGlobals

#Region "Consts"
    Friend Const TA_START_DATE_COL = "TA_Start_Date"
    Friend Const TA_END_DATE_COL = "TA_End_Date"
    Friend Const sKeyName As String = "Dashboard_Config"
    Friend Const sSectionName As String = "Settings"
#End Region

#Region "Globals"
    Friend oConn As New clsConnection
    Friend sDirectoryPath As String = ""
    Friend oFilters As New List(Of String)
    Friend sStartTA As String = ""
    Friend sEndTA As String = ""
    Friend cLog As New clsErrorlogging
    Friend dtTableOut15 As New DataTable
    Friend dtTableTaData As New DataTable
    Friend dtTableNOSF As New DataTable
    Friend dtTableNOSFTa As New DataTable
    Friend dtStoreTypes As New DataTable
#End Region

#Region "Structures"

    Friend Structure udtStoreProps
        Dim iStoreNumber As Integer
        Dim sFilename As String
        Dim sFullpath As String
        Dim sInvDate As String
        Dim sStoreType As String
    End Structure

    Friend Structure udtExceptionProps
        Dim iStoreNumber As Integer
        Dim sInvDate As String
        Dim sExclusionType As String
    End Structure

#End Region

#Region "Enums"

    Friend Enum eType
        ePCVAR = 1
        eOut16 = 2
    End Enum

    Friend Enum ePCVarColumns
        eStoreNumber = 0
        eSuperCat = 1
        eSubCat = 3
        eSKU = 4
        eItemDesc = 5
        eRetailPrice = 7
        eActualQty = 8
        eOnHand = 9
    End Enum

    Friend Enum eOut16Columns
        eInventoryDate = 0
        eStoreNumber = 1
        eRGIS_Dist = 2
        eZone = 3
        eRegion = 4
        eSKU = 5
        eItemDesc = 6
        eRetailPrice = 7
        eSuperCat = 8
        eSubCat = 9
        eOnHand = 10
        eActualQty = 11
        eBackroom = 12
    End Enum

    Friend Enum eColumnTag
        eString = 0
        eInteger = 1
        eDate = 2
    End Enum

    Friend Enum eListviewColumns
        eInvDat = 0
        eStoreNo = 1
        eStoreType = 2
        eException = 3
        eTable = 4
        eDupe = 5
        eImpDate = 6
    End Enum

    Friend Enum eDestinationTables
        Inventory_Data_TA_OUT15 = 0
        Inventory_Data_OUT15 = 1
        NOSF_Inventory_Data_TA_OUT15 = 2
        NOSF_Inventory_Data_OUT15 = 3
    End Enum

#End Region

End Module
