﻿Imports System.IO
Imports System.Data.SqlClient

Module modImportSuperCatVerifier

    Private m_DBConn As SqlConnection
    Private Property DBConn() As SqlConnection
        Get
            Return m_DBConn
        End Get
        Set(ByVal value As SqlConnection)
            m_DBConn = value
        End Set
    End Property

    Public Sub ImportSuperCatVerifier()

        Try

            Dim xsPath As String = ReadField("SELECT CustomValue FROM Admin_Config WHERE CustomName='SuperCatVerifier'")

            If Directory.Exists(xsPath) Then
                Dim xasFileList As String() = Directory.GetFiles(xsPath, "*.out36")

                '   Count the files in the folder and confirm this is correct
                If xasFileList.GetUpperBound(0) = 0 Then
                    MessageBox.Show("No files found")
                    Exit Sub
                Else
                    '   Process all the files - one by one
                    For Each xsFile As String In xasFileList
                        'txtFileNameAndPathSuperCat.Text = xsFile
                        'txtOutput.Text += vbCrLf & "Processing " & xsFile
                        StatusBarText = "Processing " & xsFile
                        ProcessSuperCatVarifierData(xsFile)
                    Next

                    MessageBox.Show("Done")
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ProcessSuperCatVarifierData(ByVal sFile As String)

        '   Get input data/Open the file
        '   Get the file name
        Dim xsRtnMsg As String = ""
        Dim xbFirstDataRowFound As Boolean
        Dim xbValid As Boolean = False
        Dim xsOpFilename As String = String.Empty

        Try

            If Not File.Exists(sFile) Then
                MessageBox.Show("File name not found. Job cancelled.")
                Return
            End If
        Catch ex As Exception
            MessageBox.Show("Problem getting the input file . Job cancelled.")
            Return
        End Try

        Dim xoSR As New StreamReader(sFile)

        xsOpFilename = sFile.Replace("\ClientData\", "\ClientData_Processed\")

        StatusBarText = "Setting up the expected columns and clearing existing data."

        '   Clear the work tables
        DBConn = New SqlConnection(oConn.ConnectionString)
        Dim xoCmd As New SqlCommand

        DBConn.Open()

        '   Open the file as a stream
        Try

            Dim xbStoreFound As Boolean = False
            Dim xbHeadingFound As Boolean = False
            Dim xnWrk As Integer = 0
            Dim xsWrk As String = ""
            Dim xnRowCount As Integer = 0
            Dim xnStore As Integer = 0
            Dim xnZone As Integer = 0
            Dim xnRegion As Integer = 0
            Dim xsEstateType As String = ""
            Dim xsStoreName As String = ""
            Dim xsSuperCatName As String = ""
            Dim xsCurrentCountYear As String = ""
            Dim xsCurrentPeriod As String = ""
            Dim xbClothing As Boolean = False
            Dim xdReportDate As Date
            Dim xbIsTA As Boolean = False

            '   The column splits are:
            '   1-40, 43-51, 53-66, 70-82, 86-99, 102-115, 118-131
            Dim xoRptCols As New RptCols
            xoRptCols.RptColsIP = New Collection
            Dim xoRptCol As New RptCol

            xoRptCol.ColHdgInput = "Super Cat Name"
            xoRptCol.ColName = "Super Cat Name"
            xoRptCol.ColStartInput = 0
            xoRptCol.ColEndInput = 40
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTString
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Super Cat"
            xoRptCol.ColName = "Super Cat"
            xoRptCol.ColStartInput = 43
            xoRptCol.ColEndInput = 51
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Counted"
            xoRptCol.ColName = "SKUs Counted"
            xoRptCol.ColStartInput = 54
            xoRptCol.ColEndInput = 66
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Unique # SKUs Checked"
            xoRptCol.ColName = "SKUs Checked"
            xoRptCol.ColStartInput = 70
            xoRptCol.ColEndInput = 82
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "% SKUs Checked"
            xoRptCol.ColName = "SKUs Checked Pcnt"
            xoRptCol.ColStartInput = 86
            xoRptCol.ColEndInput = 98
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "# of Ammendment Made"
            xoRptCol.ColName = "Changes made"
            xoRptCol.ColStartInput = 102
            xoRptCol.ColEndInput = 115
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTInteger
            xoRptCols.RptColsIP.Add(xoRptCol)

            xoRptCol = New RptCol
            xoRptCol.ColHdgInput = "Accuracy %"
            xoRptCol.ColName = "Accuracy Pcnt"
            xoRptCol.ColStartInput = 118
            xoRptCol.ColEndInput = 130
            xoRptCol.ColDataType = RptCol.ColDataTypes.DTDecimal
            xoRptCol.ColDecimals = 2
            xoRptCols.RptColsIP.Add(xoRptCol)

            Dim xsLine As String = xoSR.ReadLine()
            Do While Not xoSR.EndOfStream

                xnRowCount += 1

                '   Get the Store ID
                If Not xbStoreFound Then
                    If xnRowCount > 12 Then
                        MessageBox.Show("Store not found in 1st 12 rows. Probable data format issue. Job cancelled")
                        Exit Sub
                    End If

                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then
                        xnWrk = xsWrk.IndexOf("Store:")
                        If xnWrk > -1 Then
                            xsWrk = xsWrk.Substring(xnWrk + 7)
                            Dim xnEnd As Integer = xsWrk.IndexOf(" ")

                            If xnEnd > -1 Then
                                xsWrk = xsWrk.Substring(0, xnEnd)
                                'JR - May need to add line of code here to strip out any leading comma
                                ' before converting the string item to a store number
                                xnStore = CType(xsWrk, Integer)

                                '   Get the report date
                                xsWrk = xsLine.Substring(106, 14)
                                xdReportDate = CDate(xsWrk)

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetPeriodAndCYForDate"
                                    xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar, 10).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsCurrentCountYear = ""
                                    xsWrk = xoCmd.Parameters("Count_Year").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentCountYear = xsWrk
                                    End If

                                    xsCurrentPeriod = ""
                                    xsWrk = xoCmd.Parameters("Period_No").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsCurrentPeriod = xsWrk
                                    End If

                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStore"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_GetZoneRegionForStore"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsEstateType = xoCmd.Parameters("Estate_Type").Value.ToString
                                    If xsWrk.Length = 0 Then
                                        xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Store type for " & xnStore & ". "
                                    End If

                                    xnZone = 0
                                    xsWrk = xoCmd.Parameters("Zone").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnZone = CInt(xsWrk)
                                        If xnZone <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Zone for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xnRegion = 0
                                    xsWrk = xoCmd.Parameters("Region").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        xnRegion = CInt(xsWrk)
                                        If xnRegion <= 0 Then
                                            xsRtnMsg += "Problem during p_GetZoneRegionForStore: Missing Region for " & xnStore & ". "
                                            xbValid = False
                                        End If
                                    End If

                                    xsStoreName = ""
                                    xsWrk = xoCmd.Parameters("Store_Name").Value.ToString
                                    If xsWrk.Length > 0 Then
                                        xsStoreName = xsWrk
                                    End If

                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_IsTA"
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("InputDate", SqlDbType.Date).Value = xdReportDate
                                        xoCmd.Parameters.Add("IsTA", SqlDbType.NVarChar, 5).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                        xbIsTA = False
                                        xsWrk = xoCmd.Parameters("IsTA").Value.ToString
                                        If xsWrk.Length > 0 Then
                                            xbIsTA = CType(xsWrk, Boolean)
                                        End If

                                        '   Clear the rows (if any) that are in the table
                                        xoCmd = New SqlCommand
                                        xoCmd.Connection = DBConn
                                        xoCmd.CommandType = CommandType.StoredProcedure

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when detecting TA date. Error " & ex.Message & " "
                                    End Try

                                    '   Clear the rows (if any) that are in the table
                                    xoCmd = New SqlCommand
                                    xoCmd.Connection = DBConn
                                    xoCmd.CommandType = CommandType.StoredProcedure

                                    Try
                                        xoCmd.CommandText = "p_SuperCatVerifier_DeleteCountYearStore"
                                        xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                        xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                        xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                        xoCmd.ExecuteNonQuery()

                                    Catch ex As Exception
                                        xsRtnMsg += "Problem when clearing TB_SuperCatVerifier. Error " & ex.Message & " "
                                    End Try


                                Catch ex As Exception
                                    xsRtnMsg += "Problem when processing header data. Error " & ex.Message & " "
                                End Try

                                xbClothing = "False"

                                xbStoreFound = True

                            End If

                        End If

                    End If

                Else

                    '   Ignore blank rows (blank between chars 1-20)
                    xsWrk = xsLine.Substring(0, 20).Trim
                    If xsWrk.Length > 0 Then

                        If Not xbHeadingFound Then
                            xnWrk = xsWrk.IndexOf("------", 1)
                            If xnWrk > 0 Then
                                xbHeadingFound = True
                            End If

                        Else

                            '   Dont process a blank line or the Grand Total line
                            xsWrk = xsLine.Substring(0, 20).Trim
                            If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") = -1 Then

                                If Not xbFirstDataRowFound Then
                                    StatusBarText = "Processing report rows."
                                    xbFirstDataRowFound = True
                                End If

                                '   Confirm data format
                                Dim xnColCount As Integer = -1
                                Dim xnSuperCatID As Integer = 0
                                Dim xnSKUsCounted As Integer = 0
                                Dim xnSKUsCheckedPcnt As Decimal = 0
                                Dim xnSKUsChecked As Decimal = 0
                                Dim xnChangesMade As Integer = 0
                                Dim xnAccuracy As Decimal = 0

                                '   Load data columns

                                For Each xoCol As RptCol In xoRptCols.RptColsIP
                                    xsWrk = xsLine.Substring(xoCol.ColStartInput, xoCol.ColEndInput - xoCol.ColStartInput).Trim
                                    xoCol.ColStringValue = xsWrk

                                    xnColCount += 1
                                    Select Case xoCol.ColName
                                        Case Is = "Super Cat Name"
                                            xsSuperCatName = xoCol.ColStringValue

                                        Case Is = "Super Cat"
                                            If xsWrk.Length > 0 Then
                                                xnSuperCatID = CType(xsWrk, Integer)

                                                xoCmd = New SqlCommand
                                                xoCmd.Connection = DBConn
                                                xoCmd.CommandType = CommandType.StoredProcedure

                                                xoCmd.CommandText = "p_IsSuperCatInClothing"
                                                xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                                xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                                xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Direction = ParameterDirection.Output
                                                xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                                                xoCmd.ExecuteNonQuery()

                                                xsWrk = xoCmd.Parameters("Clothing").Value.ToString
                                                xbClothing = False
                                                If xsWrk.Length > 0 Then
                                                    If xsWrk = "1" Or xsWrk = "True" Then
                                                        xbClothing = True
                                                    End If
                                                End If

                                                xsWrk = xoCmd.Parameters("Super_Category_Name").Value.ToString
                                                xsSuperCatName = ""
                                                If xsWrk.Length > 0 Then
                                                    xsSuperCatName = xsWrk
                                                End If
                                            End If

                                        Case Is = "SKUs Counted"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCounted = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsChecked = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "SKUs Checked Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnSKUsCheckedPcnt = CType(xsWrk, Decimal)
                                            End If

                                        Case Is = "Changes made"
                                            If xsWrk.Length > 0 Then
                                                xnChangesMade = CType(xsWrk, Integer)
                                            End If

                                        Case Is = "Accuracy Pcnt"
                                            If xsWrk.Length > 0 Then
                                                xnAccuracy = CType(xsWrk, Decimal)
                                            End If
                                    End Select
                                Next

                                '   Load the data row

                                '   Write to the SuperCatVerifier table
                                xoCmd = New SqlCommand
                                xoCmd.Connection = DBConn
                                xoCmd.CommandType = CommandType.StoredProcedure

                                Try
                                    xoCmd.CommandText = "p_SuperCatVerifier_Insert"
                                    xoCmd.Parameters.Add("Count_Year", SqlDbType.NVarChar).Value = xsCurrentCountYear
                                    xoCmd.Parameters.Add("Estate_Type", SqlDbType.NVarChar, 50).Value = xsEstateType
                                    xoCmd.Parameters.Add("Zone", SqlDbType.Int).Value = xnZone
                                    xoCmd.Parameters.Add("Region", SqlDbType.Int).Value = xnRegion
                                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = xnStore
                                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 50).Value = xsStoreName
                                    xoCmd.Parameters.Add("ReportDate", SqlDbType.DateTime).Value = xdReportDate
                                    xoCmd.Parameters.Add("Period_No", SqlDbType.NVarChar).Value = xsCurrentPeriod
                                    xoCmd.Parameters.Add("Clothing", SqlDbType.Bit).Value = xbClothing
                                    xoCmd.Parameters.Add("Super_Category", SqlDbType.Int).Value = xnSuperCatID
                                    xoCmd.Parameters.Add("Super_Category_Name", SqlDbType.NVarChar, 50).Value = xsSuperCatName
                                    xoCmd.Parameters.Add("SKUsChecked", SqlDbType.Int).Value = xnSKUsChecked
                                    xoCmd.Parameters.Add("SKUsCounted", SqlDbType.Int).Value = xnSKUsCounted
                                    xoCmd.Parameters.Add("SKUsCheckedPcnt", SqlDbType.Decimal).Value = xnSKUsCheckedPcnt
                                    xoCmd.Parameters.Add("NoOfChanges", SqlDbType.Int).Value = xnChangesMade
                                    xoCmd.Parameters.Add("AccuracyPcnt", SqlDbType.Decimal).Value = xnAccuracy
                                    xoCmd.Parameters.Add("RowsUpdated", SqlDbType.Int).Direction = ParameterDirection.Output
                                    xoCmd.ExecuteNonQuery()

                                    xsWrk = xoCmd.Parameters("RowsUpdated").Value.ToString
                                    If xsWrk.Length > 0 AndAlso IsNumeric(xsWrk) Then
                                        Dim xnWrk2 As Integer = CInt(xsWrk)
                                        If xnWrk2 <> 1 Then
                                            xsRtnMsg += "Problem during p_SuperCatVerifier_Insert. "
                                            xbValid = False
                                        End If
                                    End If

                                Catch ex As Exception
                                    xsRtnMsg += "Problem when updating data. Error " & ex.Message & " "
                                    MessageBox.Show("Error during Insert: Error " & ex.Message)
                                End Try

                            End If
                        End If
                    End If

                End If

                '2014-08-14 SK
                '   When the Grand Total line is encountered, terminate the run
                If xsWrk.Length > 0 And xsLine.IndexOf("Grand Total") >= 0 Then
                    Exit Do
                End If

                xsLine = xoSR.ReadLine()

            Loop

            If xsRtnMsg.Length > 0 Then
                MsgBox(xsRtnMsg, MsgBoxStyle.Critical)
            Else
                File.Move(sFile, xsOpFilename)
            End If

        Catch ex As Exception
            MessageBox.Show("Problem getting the excel file . Job cancelled. " & ex.Message)
            Return
        Finally

            If Not IsNothing(DBConn) Then
                DBConn.Close()
                xoCmd = Nothing
                DBConn = Nothing
            End If
        End Try

    End Sub

End Module
